'use strict'

const fs = require('fs')
const bannerPicker = require('../banner-picker')
let rawdata = fs.readFileSync('./test/data.json');
let input = JSON.parse(rawdata);

QUnit.module('add', function() {
    QUnit.test('should retrieve a random banner item from the available banner items', function(assert) {
        let bannerItem = bannerPicker.getRandomBanner(input)
        let requiredProperties = ['description', 'label', 'type', 'url']
        assert.ok(typeof(bannerItem) === 'object', 'Did not retrieve a banner item object.');
        for(const property of requiredProperties) {
            assert.ok(property in bannerItem)
            assert.ok(bannerItem.property !== '')
        }
    });
    QUnit.test('should return nothing if there are no banner items', function(assert) {
        let rawdata = fs.readFileSync('./test/empty-data.json');
        let input = JSON.parse(rawdata);
        let bannerItem = bannerPicker.getRandomBanner(input)
        assert.ok(bannerItem === '', 'Should not have retrieved a banner item')
    });
    QUnit.test('should return nothing if input is not a JSON object', function(assert) {
        let bannerItem = bannerPicker.getRandomBanner('')
        assert.ok(bannerItem === '', 'Should not have retrieved a banner item')
    });
});
