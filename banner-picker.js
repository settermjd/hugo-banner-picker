/**
 * This package is a simplistic one to be used to retrieve information for rendering a banner within a Hugo site, using
 * Hugo data variables.
 * @type {{getRandomBanner: (function(*=): string|*), isValidBannerItemsList: (function(*=): boolean), getBannerItem: (function(*): *)}}
 */
module.exports = bannerPicker = {
    /**
     * Retrieves a banner item from a list of banner items, if one is available
     *
     * Here is a sample of what valid JSON data should look like:
     * [
     *   {
     *     "label": "Mezzio Essentials",
     *     "type": "book",
     *     "url": "https://...",
     *     "description": "blah blah blah"
     *   },
     *   {
     *     "label": "Mezzio Getting Started",
     *     "type": "course",
     *     "url": "https://...",
     *     "description": "blah blah blah"
     *   }
     * ]
     *
     * There is, currently, no validation done on the property values, just that the properties exist and are not empty.
     *
     * @param bannerItems Array of JSON object
     * @returns {object}
     */
    getRandomBanner: function(bannerItems) {
        return (!this.isValidBannerItemsList(bannerItems))
            ? this.getBannerItem(bannerItems)
            : ''
    },
    /**
     * Checks if the input is valid
     *
     * At the moment, the testing is really simplistic.
     * But it's enough for my needs at the current time.
     *
     * @param bannerItems
     * @returns {boolean}
     */
    isValidBannerItemsList: function(bannerItems) {
       return bannerItems.length === 0 || typeof bannerItems !== 'object'
    },
    /**
     * Retrieves a random banner item object from the list of available banner items provided
     *
     * @param bannerItems
     * @returns {object}
     */
    getBannerItem: function(bannerItems) {
        let index = Math.floor(Math.random() * bannerItems.length)
        return bannerItems[index]
    }
}
